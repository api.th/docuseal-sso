@app.route(f"{URL_PREFIX}/s/<submitter_id>/<encoded_role_external_id>")
def view_submitter(submitter_id, encoded_role_external_id):
    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    role_external_id = url_decode(encoded_role_external_id)

    response = requests.put(
        f"{DOCUSEAL_URL}/api/submitters/{submitter_id}",
        headers=DOCUSEAL_HEADERS,
        json={
            "name": user.get("name"),
            "email": user.get("email"),
            "external_id": role_external_id,
        },
    )

    submitter = response.json()
    slug = submitter.get("slug")

    return render_template(
        "view_submitter.html",
        document_url=f"{DOCUSEAL_URL}/s/{slug}",
        user=user,
    )


@app.post(f"{URL_PREFIX}/submission.new")
def new_submission():
    data = request.get_json()
    template_id = data.get("template_id")
    external_id = data.get("external_id")
    if not external_id:
        external_id = str(uuid.uuid4())

    response = requests.get(
        f"{DOCUSEAL_URL}/api/templates/{template_id}",
        headers=DOCUSEAL_HEADERS,
    )

    template = response.json()

    fields = []
    data_fields = data.get("fields")
    if data_fields:
        for field in template.get("fields"):
            field_name = field.get("name")
            if field_name not in data_fields:
                continue
            fields.append(
                {
                    "name": field_name,
                    "value": data_fields.get(field_name),
                    "readonly": True,
                }
            )

    submitters = []
    for submitter in template.get("submitters"):
        role = submitter.get("name")
        role_external_id = f"{external_id}@{role}"
        submitters.append(
            {
                "role": role,
                "email": "someone@api.co.th",
                "external_id": role_external_id,
            }
        )

    post_data = {}
    post_data.update(data)
    post_data.update(
        {
            "send_email": False,
            "send_sms": False,
            "external_id": external_id,
            "fields": fields,
            "submitters": submitters,
        }
    )

    response = requests.post(
        f"{DOCUSEAL_URL}/api/submissions",
        headers=DOCUSEAL_HEADERS,
        json=post_data,
    )

    result = []
    data = response.json()
    for entry in data:
        role_external_id = entry.get("external_id")
        _, role = role_external_id.rsplit("@", 1)
        entry["role"] = role
        encoded_role_external_id = url_encode(role_external_id)

        submitter_id = entry.get("id")
        entry["sso_url"] = (
            f"{DOCUSEAL_URL}/id.api/s/{submitter_id}/{encoded_role_external_id}"
        )

        slug = entry.get("slug")
        entry["external_url"] = f"{DOCUSEAL_URL}/s/{slug}"
        result.append(entry)

    return jsonify(result)
