import re
import os
import requests
import uuid
from flask import (
    Flask,
    redirect,
    url_for,
    session,
    request,
    render_template,
    jsonify,
    abort,
    send_from_directory,
)

from authlib.integrations.flask_client import OAuth, token_update
from urllib.parse import quote_plus as url_encode, unquote_plus as url_decode
from decouple import config

DEBUG = config("DEBUG", cast=bool)

PAGE_NOT_FOUND = 404
FORBIDDEN = 403

URL_PREFIX = config("URL_PREFIX", default="")

DOCUSEAL_URL = config("DOCUSEAL_URL")
DOCUSEAL_HEADERS = {
    "X-Auth-Token": config("DOCUSEAL_AUTH_TOKEN"),
    "content-type": "application/json",
}

CLIENT_ID = config("OAUTH_CLIENT_ID")
CLIENT_SECRET = config("OAUTH_CLIENT_SECRET")
OAUTH_END_SESSION_ENDPOINT = config("OAUTH_END_SESSION_ENDPOINT")

OUR_EMAIL_DOMAIN = "api.co.th"
ANY_CURRENT_USER_EMAIL = "me@api.co.th"

app = Flask(__name__)
app.secret_key = config("SECRET_KEY")

oauth = OAuth(app=app)
oauth.register(
    name="keycloak",
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET,
    server_metadata_url=config("OAUTH_META_DATA_URL"),
    client_kwargs=dict(
        scope=config("OAUTH_SCOPE"),
        code_challenge_method=config("OAUTH_CODE_CHALLENGE_METHOD"),
    ),
)


def GET(key, default=None):
    value = request.args.get(key)
    if value:
        return url_decode(value)
    return default


@app.route(f"{URL_PREFIX}/static/<path:path>")
def serve_static(path):
    root, file_name = path.rsplit("/", 1)
    directory = os.path.join("static", root)
    return send_from_directory(directory, file_name)


@app.route(f"{URL_PREFIX}/login")
def login():
    next_uri = request.args.get("next", default="")
    session["login_next_uri"] = next_uri
    redirect_uri = url_for("auth", _external=True)
    return oauth.keycloak.authorize_redirect(redirect_uri)


@app.route(f"{URL_PREFIX}/logout")
def logout():
    session.pop("user", None)
    return redirect(OAUTH_END_SESSION_ENDPOINT)


@app.route(f"{URL_PREFIX}/auth")
def auth():
    token = oauth.keycloak.authorize_access_token()
    session["user"] = token["userinfo"]

    next_uri = session.get("login_next_uri")
    if not next_uri:
        return redirect("/")
    return redirect(url_decode(next_uri))


"""
user {   
    'acr': '0', 'at_hash': 'LWWPbZhFD108n7ci28NdXw', 'aud': 'plasticsign.api', 
    'auth_time': 1711029762, 'azp': 'plasticsign.api', 
    'email': 'sittipon@api.co.th', 
    'email_verified': False, 
    'exp': 1711030735, 
    'family_name': 'สิมะสันติ', 
    'given_name': 'สิทธิพล', 
    'iat': 1711030435, 
    'iss': 'https://id.api.co.th/realms/Apisith', 
    'jti': '0b8e7fe7-1b04-4d19-b39b-5cb8520ac4f8', 
    'name': 'สิทธิพล สิมะสันติ', 
    'nonce': 'N3fGxAFIDCzJoVNq55Bi', 
    'preferred_username': 'sittipon', 
    'roles': [
        'PD-Control', 'ocisSpaceAdmin', 'DDC', 
        'HR-OfficePayroll', 'MR', 'MT', 'admin', 
        'HR', 'IT', 'users', 'PD', 'offline_access', 
        'PH', 'uma_authorization', 'ocisUser', 
        'HR-Payroll', 'ocisAdmin', 'MK', 
        'playbook', 'default-roles-apisith'
    ], 
    'session_state': '2eb92e1f-c7ee-4f4e-b80f-f5a26117a46e', 
    'sid': '2eb92e1f-c7ee-4f4e-b80f-f5a26117a46e', 
    'sub': 'eae11eff-19d3-4b78-969e-3ac7bf4678d0', 
    'typ': 'ID'
}
"""


def get_template_preview_image_url(entry):
    documents = entry.get("documents") or []
    for document in documents:
        image_url = document.get("preview_image_url")
        if not image_url:
            continue
        return image_url


def list_templates_in_folder(folder_name):
    response = requests.get(
        f"{DOCUSEAL_URL}/api/templates?folder={folder_name}",
        headers=DOCUSEAL_HEADERS,
    )

    body = response.json()
    entries = body.get("data") or []
    for entry in entries:
        template_id = entry.get("id")
        author = entry.get("author") or {}
        yield {
            "id": template_id,
            "slug": entry.get("slug"),
            "name": entry.get("name"),
            "preview_image_url": get_template_preview_image_url(entry),
            "url": f"{URL_PREFIX}/folders/{folder_name}/template-{template_id}",
            "author": {
                "id": author.get("id"),
                "first_name": author.get("first_name"),
                "last_name": author.get("last_name"),
                "email": author.get("email"),
            },
        }


@app.route(f"{URL_PREFIX}/folders/<folder_name>")
def view_folder(folder_name):
    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    decoded_folder_name = url_decode(folder_name)
    templates = list(list_templates_in_folder(folder_name))

    return render_template(
        "view_folder.html",
        folder_name=decoded_folder_name,
        templates=templates,
        user=user,
    )


EMAIL_PATTERN = re.compile(r"[\w.+-]+@[\w-]+\.[\w.-]+")


def find_role_email(text):
    match = EMAIL_PATTERN.search(text)
    if match:
        return match.group(0)
    if "(Me)" in text:
        return ANY_CURRENT_USER_EMAIL
    return None


def find_role_group(text):
    if "." not in text:
        return 0
    group, _ = text.split(".", 1)
    number = group.strip()
    if not number:
        return 0

    try:
        return int(number)
    except:
        return 0


def get_slug(text):
    text = text.replace(" ", "_")
    text = text.lower()
    return text


def get_template(template_id, user_email=None):
    response = requests.get(
        f"{DOCUSEAL_URL}/api/templates/{template_id}",
        headers=DOCUSEAL_HEADERS,
    )

    data = response.json()

    fields = []
    entries = data.get("fields") or []
    for entry in entries:
        field_type = entry.get("type")
        if field_type == "signature":
            continue
        name = entry.get("name")
        if not name:
            continue
        fields.append(
            {
                "name": name,
                "uuid": entry.get("uuid"),
                "submitter_uuid": entry.get("submitter_uuid"),
                "required": entry.get("required"),
                "type": entry.get("type"),
                "preferences": entry.get("preferences"),
                "areas": entry.get("areas"),
            }
        )

    submitters = []
    entries = data.get("submitters") or []
    for entry in entries:
        role = entry.get("name")
        default_email = find_role_email(role)
        if default_email == ANY_CURRENT_USER_EMAIL:
            if user_email:
                default_email = user_email

        role_title = role
        if default_email:
            role_title = role_title.rsplit("(", 1)[0].strip()

        submitters.append(
            {
                "uuid": entry.get("uuid"),
                "role": role,
                "title": role_title,
                "email": default_email or "",
            }
        )

    return {
        "id": data.get("id"),
        "slug": data.get("slug"),
        "name": data.get("name"),
        "submitters": submitters,
        "fields": fields,
        "preview_image_url": get_template_preview_image_url(data),
    }


def create_submission(template_id=None, external_id=None, fields=None, submitters=None):
    if not external_id:
        external_id = str(uuid.uuid4()).replace("-", "")

    for submitter in submitters:
        submitter.update(
            {
                "completed_redirect_url": f"{DOCUSEAL_URL}{URL_PREFIX}/completed/submission-{url_encode(external_id)}"
            }
        )

    response = requests.post(
        f"{DOCUSEAL_URL}/api/submissions",
        headers=DOCUSEAL_HEADERS,
        json={
            "template_id": template_id,
            "send_email": False,
            "send_sms": False,
            "external_id": external_id,
            "fields": fields,
            "submitters": submitters,
        },
    )

    for entry in response.json():
        submission_id = entry.get("submission_id")
        return {
            "id": submission_id,
            "url": f"{DOCUSEAL_URL}{URL_PREFIX}/submission-{submission_id}",
        }


@app.post(f"{URL_PREFIX}/folders/<folder_name>/template-<template_id>")
@app.post(f"{URL_PREFIX}/folders/<folder_name>/template-<template_id>/<external_id>")
def new_submission(folder_name, template_id, external_id=None):
    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    template = get_template(template_id)

    submitters = []
    for submitter in template.get("submitters"):
        role = submitter.get("name")
        key = submitter.get("uuid")
        email = request.form.get(f"submitter_{key}")
        submitters.append(
            {
                "role": role,
                "email": email,
            }
        )

    fields = []
    for field in template.get("fields"):
        key = field.get("uuid")
        field_name = field.get("name")
        field_value = request.form.get(f"field_{key}") or ""
        if not field_value:
            continue
        fields.append(
            {
                "name": field_name,
                "value": field_value,
                "readonly": True,
            }
        )

    decoded_external_id = None
    if external_id:
        decoded_external_id = url_decode(external_id)

    submission = create_submission(
        template_id=template_id,
        external_id=decoded_external_id,
        fields=fields,
        submitters=submitters,
    )

    on_submission_started(submission.get("id"))

    return redirect(submission.get("url"))


@app.get(f"{URL_PREFIX}/folders/<folder_name>/template-<template_id>")
@app.get(f"{URL_PREFIX}/folders/<folder_name>/template-<template_id>/<external_id>")
def view_template(folder_name, template_id, external_id=None):
    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    template = get_template(template_id, user_email=user["email"])

    for index, submitter in enumerate(template.get("submitters")):
        value = GET("submitter_%ld" % (index + 1))
        if value:
            submitter["email"] = value

    fields = []
    for field in template.get("fields"):
        field_name = field.get("name")
        fields.append(
            {
                "name": field_name,
                "uuid": field.get("uuid"),
                "value": GET(field_name) or GET(get_slug(field_name)) or "",
            }
        )

    slug = template.get("slug")

    return render_template(
        "view_template.html",
        template=template,
        folder_name=url_decode(folder_name),
        folder_url=f"{URL_PREFIX}/folders/{folder_name}",
        fields=fields,
        document_url=f"{DOCUSEAL_URL}/d/{slug}",
        user=user,
    )


def filter_current_uncompleted_submitters(submitters):
    min_uncompleted_role_group = 100000
    for submitter in submitters:
        status = submitter.get("status")
        if status == "completed":
            continue

        submitter_role = submitter.get("role")
        role_group = find_role_group(submitter_role)
        if min_uncompleted_role_group > role_group:
            min_uncompleted_role_group = role_group

    for submitter in submitters:
        status = submitter.get("status")
        if status == "completed":
            continue

        role_group = find_role_group(submitter.get("role"))
        if role_group > min_uncompleted_role_group:
            continue

        yield submitter


# get_email_role('ddc@api.co.th') -> ddc
def get_email_role(email):
    if not email:
        return
    if "@" not in email:
        return
    role_name, domain_name = email.lower().split("@")
    if domain_name != OUR_EMAIL_DOMAIN:
        return
    return role_name


def get_submission(submission_id):
    response = requests.get(
        f"{DOCUSEAL_URL}/api/submissions/{submission_id}",
        headers=DOCUSEAL_HEADERS,
    )
    return response.json()


def is_email_equal(rule_email, user_email=None):
    if not rule_email:
        return False
    if not user_email:
        return False
    if rule_email == user_email:
        return True
    if rule_email.lower() == user_email.lower():
        return True
    return False


def find_related_submitter(submission_id, user=None):
    data = get_submission(submission_id)
    submitters = data.get("submitters") or []

    need_submitters = list(filter_current_uncompleted_submitters(submitters))

    # Check if there is a submitter with ANY_CURRENT_USER_EMAIL
    for submitter in need_submitters:
        email = find_role_email(submitter.get("role"))
        if email != ANY_CURRENT_USER_EMAIL:
            continue

        return submitter, False

    # Check if there is a submitter for this user_email that need to sign
    user_email = user.get("email")
    for submitter in need_submitters:
        email = submitter.get("email")
        if not is_email_equal(email, user_email=user_email):
            continue

        return submitter, False

    # Check if there is a submitter for current user role that need to sign
    user_roles = [x.lower() for x in user.get("roles")]
    for submitter in need_submitters:
        submitter_email = find_role_email(submitter.get("role"))
        email_role = get_email_role(submitter_email)
        if not email_role:
            continue

        if email_role not in user_roles:
            continue

        return submitter, False

    # Check if there is a submitter for this user_email that has already signed
    user_email = user.get("email")
    for submitter in submitters:
        email = submitter.get("email")
        if email != user_email:
            continue

        return submitter, True

    return None, False


def update_submitter(submitter, data=None):
    submitter_id = submitter.get("id")
    requests.put(
        f"{DOCUSEAL_URL}/api/submitters/{submitter_id}",
        headers=DOCUSEAL_HEADERS,
        json=data,
    )


def update_submitter_user(submitter, user=None):
    if not user:
        return

    user_name = user.get("name")
    user_email = user.get("email")
    submitter_name = submitter.get("name")
    submitter_email = submitter.get("email")

    if user_name == submitter_name:
        if user_email == submitter_email:
            return

    update_submitter(
        submitter,
        data={
            "name": user_name,
            "email": user_email,
        },
    )


def get_submission_id(submission_id):
    try:
        return int(submission_id)
    except:
        pass

    response = requests.get(
        f"{DOCUSEAL_URL}/api/submissions?external_id={submission_id}",
        headers=DOCUSEAL_HEADERS,
    )

    body = response.json()
    for entry in body.get("data"):
        return entry.get("id")


def email_submitter(submitter, submission=None):
    pass


def post_discord_submitter(submitter, submission=None):
    pass


def notify_submitter(submitter, submission=None):
    email = submitter.get("email")
    if not email:
        raise Exception("No email")

    if "@" not in email:
        raise Exception("Not a valid email")

    _, domain_name = email.split("@", 1)
    if domain_name != OUR_EMAIL_DOMAIN:
        email_submitter(submitter, submission=submission)
        return

    post_discord_submitter(submitter, submission=submission)


def notify_current_submitters_group(submission_id):
    submission = get_submission(submission_id)
    submitters = submission.get("submitters") or []
    need_submitters = filter_current_uncompleted_submitters(submitters)
    for submitter in need_submitters:
        email = submitter.get("email")
        if not email:
            continue
        if submitter.get("external_id") == "sent":
            continue

        try:
            notify_submitter(submitter, submission=submission)
        except:
            continue
        update_submitter(
            submitter,
            data={
                "external_id": "sent",
            },
        )


def on_submission_started(submission_id):
    notify_current_submitters_group(submission_id)


@app.route(f"{URL_PREFIX}/completed/submission-<submission_id>")
def on_submitter_completed(submission_id):
    notify_current_submitters_group(submission_id)
    return redirect(f"{URL_PREFIX}/submission-{submission_id}")


@app.route(f"{URL_PREFIX}/accounts/dashboard")
def view_account_dashboard():
    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    home_url = GET("home", default="/")
    return render_template(
        "accounts/dashboard.html",
        user=user,
        accounts_dashboard_url="#",
        home_url=home_url,
        logout_url=f"{URL_PREFIX}/logout",
    )


@app.route(f"{URL_PREFIX}/submission-<submission_id>")
def view_submission(submission_id):
    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    submission_id = get_submission_id(submission_id)
    submitter, completed = find_related_submitter(submission_id, user=user)
    if not submitter:
        return abort(FORBIDDEN)

    update_submitter_user(submitter, user=user)
    slug = submitter.get("slug")
    return render_template(
        "view_submitter.html",
        document_url=f"{DOCUSEAL_URL}/s/{slug}",
        user=user,
    )


def get_submitter(submitter_id):
    response = requests.get(
        f"{DOCUSEAL_URL}/api/submitters/{submitter_id}",
        headers=DOCUSEAL_HEADERS,
    )

    body = response.json()
    return body


def is_our_email(email):
    if not email:
        return False

    domain = email.split("@", 1)[-1]
    return domain.lower() == OUR_EMAIL_DOMAIN.lower()


@app.route(f"{URL_PREFIX}/submitter-<submitter_id>/<submitter_slug>")
def view_submitter(submitter_id, submitter_slug):
    submitter = get_submitter(submitter_id)
    if submitter["slug"] != submitter_slug:
        return abort(PAGE_NOT_FOUND)

    email = submitter["email"]
    if not is_our_email(email):
        return redirect(f"{DOCUSEAL_URL}/s/{submitter_slug}")

    user = session.get("user")
    if not user:
        login_url = url_for("login", next=url_encode(request.url))
        return redirect(login_url)

    user_email = user.get("email") or ""
    if not is_email_equal(email, user_email=user_email):
        submission_id = submitter.get("submission_id")
        return redirect(f"{URL_PREFIX}/submission-{submission_id}")

    update_submitter_user(submitter, user=user)
    return render_template(
        "view_submitter.html",
        document_url=f"{DOCUSEAL_URL}/s/{submitter_slug}",
        user=user,
        accounts_dashboard_url=f"{URL_PREFIX}/accounts/dashboard?home={url_encode(request.url)}",
    )


if __name__ == "__main__":
    app.run(debug=DEBUG)
