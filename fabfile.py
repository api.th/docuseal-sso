from fabric.api import env, local, run, settings, cd, get, put
from fabric.contrib.files import exists
from plugins.vault import Vault, write_file, read_file

import datetime
import os


def once(func):
    func.run = False

    def wrapper():
        if func.run:
            return
        func.run = True
        return func()

    return wrapper


@once
def pullsshkey():
    key_file = os.path.expanduser("~/.ssh/id_ed25519")
    if not os.path.exists(key_file):
        local('ssh-keygen -t ed25519 -q -N "" -f %s' % key_file)
    pub_file = os.path.expanduser("~/.ssh/id_ed25519.pub")
    cert_file = os.path.expanduser("~/.ssh/id_ed25519-cert.pub")
    vault = Vault()
    cert_content = vault.sign_ssh_key(
        "docuseal_sso", mount="playbook-ssh", public_key_file=pub_file
    )
    write_file(cert_file, content=cert_content)


def media02():
    pullsshkey()
    env.branch = "production"
    env.pip = "/opt/docuseal_sso/.env/bin/pip"
    env.hosts = [
        "docuseal_sso@media02.api.co.th",
    ]


@once
def merge():
    if env.branch and env.branch != "main":
        local("git checkout %(branch)s" % env)
        local("git merge main")
        local("git checkout main")


@once
def push():
    with settings(warn_only=True):
        local("git push origin %(branch)s" % env, capture=False)


def deployapp():
    with cd("/opt/docuseal_sso/app"):
        run("git pull origin %(branch)s" % env)


def deploypython():
    with cd("/opt/docuseal_sso/app"):
        run("%(pip)s install --upgrade pip" % env)
        run("%(pip)s install -r web/requirements.txt" % env)


@once
def reloadweb():
    run("sudo /usr/bin/supervisorctl restart docuseal_sso-web" % env)


@once
def deploy():
    push()
    deployapp()
    deploypython()
    reloadweb()
