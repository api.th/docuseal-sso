

WEB_PIP = "web/.env/bin/pip"
DEV_PIP = ".envdev/bin/pip"
FAB = ".envdev/bin/fab"

def command?(command)
    system("which #{ command} > /dev/null 2>&1")
end


def python(version)
    if !Dir.exist?(File.expand_path("~/.pyenv"))
        sh "git clone https://github.com/pyenv/pyenv.git ~/.pyenv"
    end
    if !command?("make")
        sh "sudo apt update"
        sh "sudo apt install -y build-essential libbz2-dev libffi-dev libfreetype6-dev liblcms2-dev libldap2-dev libncurses-dev libpq-dev libreadline-dev libsasl2-dev libsqlite3-dev libssl-dev libtiff-dev libwebp-dev tcl8.6-dev tk8.6-dev zlib1g-dev postgresql-client python3-psycopg2 python-setuptools python-tk python3-dev"
    end
    if !File.exist?(File.expand_path("~/.pyenv/versions/#{version}/bin/python"))
        sh "~/.pyenv/bin/pyenv install #{version}"
    end
    
    File.expand_path("~/.pyenv/versions/#{version}/bin/python")
end


namespace :dev do
    task :pythonenv do
        if !File.exist?(".envdev")
            version = File.read(".python-version").strip
            sh "#{python(version)} -m venv .envdev"
            sh "#{DEV_PIP} install --upgrade pip"
            sh "#{DEV_PIP} install wheel setuptools"
        end 
    end

    task :packages do
        sh "#{DEV_PIP} install -r requirements-dev.txt"
    end

    task :env => [
        :pythonenv,
        :packages,
    ]


end

namespace :web do

    task :pythonenv do
        if !File.exist?("web/.env")
            Dir.chdir("web") do
                version = File.read(".python-version").strip
                sh "#{python(version)} -m venv .env"
            end

            sh "#{WEB_PIP} install --upgrade pip"
            sh "#{WEB_PIP} install wheel setuptools"
        end 
    end

    task :sass do
        if !command?("npm")
            sh "sudo apt update"
            sh "sudo DEBIAN_FRONTEND=noninteractive apt install --yes npm"
        end
        if !command?("sass")
            sh "sudo npm -g install sass"
        end

        sh "sass --watch --load-path=web --style compressed web/static/"
    end

    task :env => [
        :pythonenv,
    ]

    task :build => "dev:env" do
        sh ".envdev/bin/ruff format ."
    end

    task :run => :env do
        sh "rake web:build"
        sh "#{WEB_PIP} install -r web/requirements.txt"

        Dir.chdir("web") do
            sh ".env/bin/python server.py"
        end
    end
end


namespace :deploy do
    task :media02 => "dev:env" do
        sh "#{FAB} media02 merge deploy"
    end

    task :all => [
        :media02
    ]
end